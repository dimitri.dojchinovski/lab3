package com.example.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/PizzaOrder.do")
public class PizzaOrder extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)throws IOException, ServletException{
        String pizzaSize = request.getParameter("size");
        request.getSession().setAttribute("pizzaSize", pizzaSize);
        response.sendRedirect("DeliveryInfo.html");
    }

}
